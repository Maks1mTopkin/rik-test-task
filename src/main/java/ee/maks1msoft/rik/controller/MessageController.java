package ee.maks1msoft.rik.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@RestController
@RequestMapping("/api")
public class MessageController {

    @GetMapping("/messages")
    public Map<String, String> getMessages(@RequestParam(required = false) String section) {
        Properties properties = new Properties();
        Map<String, String> messages = new HashMap<>();

        try (InputStream input = getClass().getClassLoader().getResourceAsStream("messages.properties")) {
            if (input == null) {
                throw new IOException("Could not find the properties file");
            }

            properties.load(input);
            for (String key : properties.stringPropertyNames()) {
                String newKey = key;
                if (section == null || key.startsWith(section + ".")) {
                    if (key.indexOf('.') != -1) {
                        newKey = key.substring(key.indexOf('.') + 1);
                    }
                    messages.put(newKey, properties.getProperty(key));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return messages;
    }
}

