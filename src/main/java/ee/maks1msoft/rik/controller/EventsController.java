package ee.maks1msoft.rik.controller;

import ee.maks1msoft.rik.model.Event;
import ee.maks1msoft.rik.service.EventsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EventsController {

    @Autowired
    private EventsService eventsService;

    @GetMapping("/getAllEvents")
    public List<Event> getAllEvents() {
        return eventsService.getAllEvents();
    }


}
