package ee.maks1msoft.rik.controller;

import ee.maks1msoft.rik.model.Event;
import ee.maks1msoft.rik.model.EventDto;
import ee.maks1msoft.rik.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class Entertainment extends BaseController {

    @Autowired
    private EventService eventService;

    @GetMapping("/urituse_lisamine")
    public String Entertainment(Model model) {
        addCommonAttributes(model);
        return "entertainment";
    }

    @PostMapping("/addEvent")
    public String addEvent(@ModelAttribute EventDto eventDto, Model model) {
        System.out.println(eventDto);
        Event newEvent = eventService.createEvent(eventDto);

        return "redirect:/mainPage";
    }

}
