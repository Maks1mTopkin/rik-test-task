package ee.maks1msoft.rik.repository;

import ee.maks1msoft.rik.model.IndividualParticipant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IndividualParticipantRepository extends JpaRepository<IndividualParticipant, Long> {
    List<IndividualParticipant> findByEvent_Id(Long eventId);
}
