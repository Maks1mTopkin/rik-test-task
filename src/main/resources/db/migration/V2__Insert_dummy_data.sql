
INSERT INTO event (name, event_time, location, additional_info)
VALUES ('Uritus 1', '2022-12-01 14:00:00', 'Tallinn', 'Lisa Info 1'),
       ('Uritus 2', '2023-10-12 18:00:00', 'Tartu', 'Lisa Info 2');

INSERT INTO individual_participant (first_name, last_name, payment_method, additional_info, personal_code, event_id)
VALUES ('Name', 'Surname', 'Sulas', 'Fuusiline isik', '49002011234', 1),
       ('Maria', 'Petrova', 'Kreedit kaart', 'Fuusiline isik', '49002011235', 2);

INSERT INTO company_participant (company_name, payment_method, additional_info, company_reg_code, event_id)
VALUES ('OU Ivanovy', 'Ulekande', 'Firma', '12345678', 1),
       ('OU Petrovu', 'Ulekande', 'Fuusiline isik', '87654321', 2);
