package ee.maks1msoft.rik.service;

import ee.maks1msoft.rik.model.CompanyParticipant;
import ee.maks1msoft.rik.model.Event;
import ee.maks1msoft.rik.model.EventDto;
import ee.maks1msoft.rik.model.IndividualParticipant;
import ee.maks1msoft.rik.repository.CompanyParticipantRepository;
import ee.maks1msoft.rik.repository.IndividualParticipantRepository;
import ee.maks1msoft.rik.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Service
public class EventService {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private CompanyParticipantRepository companyParticipantRepository;

    @Autowired
    private IndividualParticipantRepository individualParticipantRepository;

    public Event createEvent(EventDto eventDto) {
        Event newEvent = new Event();
        newEvent.setName(eventDto.getName());

        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy' 'HH:mm:ss");
        LocalDateTime parsedDateTime = LocalDateTime.parse(eventDto.getEventTime(), inputFormatter);

        newEvent.setEventTime(parsedDateTime);
        newEvent.setLocation(eventDto.getLocation());
        newEvent.setAdditionalInfo(eventDto.getAdditionalInfo());

        return eventRepository.save(newEvent);
    }

    public void deleteEvent(Long id) {
        eventRepository.deleteById(id);
    }

    public void addCompanyParticipantToEvent(Long eventId, CompanyParticipant companyParticipant) {
        Optional<Event> eventOptional = eventRepository.findById(eventId);
        if (eventOptional.isPresent()) {
            Event event = eventOptional.get();
            companyParticipant.setEvent(event);
            companyParticipantRepository.save(companyParticipant);
        }
    }

    public void addIndividualParticipantToEvent(Long eventId, IndividualParticipant individualParticipant) {
        Optional<Event> eventOptional = eventRepository.findById(eventId);
        if (eventOptional.isPresent()) {
            Event event = eventOptional.get();
            individualParticipant.setEvent(event);
            individualParticipantRepository.save(individualParticipant);
        }
    }
}
