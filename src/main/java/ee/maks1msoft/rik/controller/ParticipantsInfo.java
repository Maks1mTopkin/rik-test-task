package ee.maks1msoft.rik.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ParticipantsInfo extends BaseController {

    @GetMapping("/osavotja_info")
    public String contactPage(Model model) {
        addCommonAttributes(model);
        return "participantsInfo";
    }
}
