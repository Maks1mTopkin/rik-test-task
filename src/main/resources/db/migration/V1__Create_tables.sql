CREATE TABLE IF NOT EXISTS event
(
    id              BIGINT AUTO_INCREMENT PRIMARY KEY,
    name            VARCHAR(255) NOT NULL,
    event_time      TIMESTAMP    NOT NULL,
    location        VARCHAR(255) NOT NULL,
    additional_info VARCHAR(1000)
    );

CREATE TABLE IF NOT EXISTS individual_participant
(
    id              BIGINT AUTO_INCREMENT PRIMARY KEY,
    first_name      VARCHAR(255) NOT NULL,
    last_name       VARCHAR(255) NOT NULL,
    payment_method  VARCHAR(255) NOT NULL,
    additional_info VARCHAR(1000),
    personal_code   VARCHAR(20) NOT NULL,
    event_id        BIGINT,
    FOREIGN KEY (event_id) REFERENCES event (id)
    );

CREATE TABLE IF NOT EXISTS company_participant
(
    id              BIGINT AUTO_INCREMENT PRIMARY KEY,
    company_name    VARCHAR(255) NOT NULL,
    payment_method  VARCHAR(255) NOT NULL,
    additional_info VARCHAR(1000),
    company_reg_code VARCHAR(20) NOT NULL,
    event_id        BIGINT,
    FOREIGN KEY (event_id) REFERENCES event (id)
    );
