var createFooter = function () {
    var loorem = document.getElementById(LOOREM)
    var ipsum = document.getElementById(IPSUM)
    var contact1 = document.getElementById(KONTAKT_1)
    var contact2 = document.getElementById(KONTAKT_2)

    var looremLinks = filterKeysByPrefix(bundle.footerTxt, LOOREM)
    createLinksFromObject(looremLinks, LOOREM, loorem)

    var ipsumLinks = filterKeysByPrefix(bundle.footerTxt, IPSUM)
    createLinksFromObject(ipsumLinks, IPSUM, ipsum)

    var contact1Links = filterKeysByPrefix(bundle.footerTxt, KONTAKT_1)
    createLinksFromObject(contact1Links, KONTAKT_1, contact1)

    var contact2Links = filterKeysByPrefix(bundle.footerTxt, KONTAKT_2)
    createLinksFromObject(contact2Links, KONTAKT_2, contact2)

}