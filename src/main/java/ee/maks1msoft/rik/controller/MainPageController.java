package ee.maks1msoft.rik.controller;

import ee.maks1msoft.rik.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;


@Controller
public class MainPageController extends BaseController {

    @Autowired
    private EventService eventService;

    @GetMapping("/avaleht")
    public String mainPage(Model model) {
        addCommonAttributes(model);
        return "mainPage";
    }

//    @DeleteMapping("/deleteEvent/{id}")
    @PostMapping("/deleteEvent/{id}")
    public String deleteEvent(@PathVariable Long id) {
        eventService.deleteEvent(id);
        return "redirect:/avaleht";
    }
}
