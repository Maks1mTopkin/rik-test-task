# Use an official maven base image with JDK 17
FROM maven:3.8.3-openjdk-17-slim as build

# Set the working directory
WORKDIR /usr/src/app

# Copy the current directory contents into the container
COPY . .

# Build the application
RUN mvn clean package -DskipTests

# Use an official Tomcat base image
FROM tomcat:9.0-jdk17-openjdk-slim

# Copy built war file into Tomcat
COPY --from=build /usr/src/app/target/*.war /usr/local/tomcat/webapps/

# Rename the WAR file
RUN mv /usr/local/tomcat/webapps/*.war /usr/local/tomcat/webapps/rik.war

# Make port 8080 available to the world outside this container
EXPOSE 8080
