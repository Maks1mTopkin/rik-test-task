var doRequest = function (args, cbk) {
    cbk = typeof cbk === 'function' ? cbk : function (data) {
    };
    applyIf(args, {
        method: "GET",
        url: "",
        params: {}
    });

    const urlParams = new URLSearchParams();
    if (typeof args.params === 'object') {
        for (const [key, value] of Object.entries(args.params)) {
            urlParams.append(key, value);
        }
    }
    const fullUrl = `${args.url}?${urlParams}`;

    const xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                try {
                    const myObj = JSON.parse(this.responseText);
                    cbk(myObj);
                } catch (e) {
                    cbk({error: 'Invalid JSON'});
                }
            } else {
                cbk({error: 'Request failed'});
            }
        }
    };

    xhttp.onerror = function () {
        cbk({error: 'Request failed'});
    };

    xhttp.onabort = function () {
        cbk({error: 'Request aborted'});
    };

    xhttp.ontimeout = function () {
        cbk({error: 'Request timed out'});
    };

    xhttp.open(args.method, fullUrl, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
};
var applyIf = function (args, defaults) {
    for (let key in defaults) {
        if (defaults.hasOwnProperty(key) && (!args.hasOwnProperty(key) || args[key] === null)) {
            args[key] = defaults[key];
        }
    }
};

var jsLoader = new function () {
    var loadedFiles = {};

    this.load = function (url, onLoadCbk) {
        if (typeof loadedFiles[url] === 'undefined') {
            var script = document.createElement('script');
            script.src = url;
            script.onload = function () {
                loadedFiles[url] = true;
                if (typeof onLoadCbk === 'function') {
                    onLoadCbk(jsLoader);
                }
            };
            document.getElementsByTagName('head')[0].appendChild(script);
        }
    };

    this.isLoaded = function (url) {
        return !!loadedFiles[url];
    };

    this.filesCount = function () {
        return Object.keys(loadedFiles).length;
    }
};

var require = function (url, onLoadCbk) {
    jsLoader.load(url, onLoadCbk);
};

var init = function () {
    var loaded = function (jsLoader) {
        if (jsLoader.filesCount() == 7) {
            setActive();
        }
    };

    require('messages.js', loaded);
    require('header.js', loaded);
    require('footer.js', loaded);
    require('constants.js', loaded);
    require('navigation.js', loaded);
    require('mainPage.js', loaded);
    require('entertainment.js', loaded);
};

function setActive() {
    const currentPath = window.location.pathname;
    const navItems = document.querySelectorAll('[data-path]');

    navItems.forEach((item) => {
        item.classList.remove('active');
        if (item.getAttribute('data-path').includes(currentPath)) {
            item.classList.add('active');
        }
    });
}

var getBlock = function (id) {
    return null != id ? document.getElementById(id) : null;
};

var createBlock = function (args) {
    applyIf(args, {
        id: '',
        cls: '',
        elem: 'div'
    });

    var block = document.createElement(args.elem);

    if (args.hasOwnProperty('id')) {
        block.id = args.id;
    }

    if (args.hasOwnProperty('cls')) {
        block.className = args.cls;
    }

    block.setText = function (txt) {
        if (null != txt && txt.length > 0) {
            this.appendChild(document.createTextNode(txt));
        } else {
            if (null != this.firstChild) {
                this.firstChild.remove();
            }
        }
    };

    block.getValue = function () {
        return this.value;
    };

    block.setValue = function (val) {
        this.value = val;
    };

    block.removeBlock = function () {
        this.remove();
    };

    block.setOnClickEvent = function (cbk) {
        if (null != cbk) {
            this.addEventListener("click", cbk);
        }
    };

    block.setEnabled = function (bEnable) {
        this.disabled = !bEnable;
    };

    block.appendBlock = function (block) {
        if (null != block) {
            this.appendChild(block);
        }
    };

    block.setClass = function (arg) {
        this.className = arg;
    };

    return block;
}

var filterKeysByPrefix = function (obj, prefix) {
    var filteredObj = {};
    for (var key in obj) {
        if (key.startsWith(prefix)) {
            filteredObj[key] = obj[key];
        }
    }
    return filteredObj;
}

var createLinksFromObject = function (obj, prefix, targetElement) {
    const sortOrder = ['town', 'adress', 'telefon', 'faks'];
    const elementsArray = [];

    for (let key in obj) {
        if (key.startsWith(prefix) && !key.endsWith('.link')) {
            const text = obj[key];
            const link = obj[key + '.link'];

            let elem;
            if (link) {
                const linkElem = createBlock({ elem: 'a' });
                linkElem.href = link;
                linkElem.target = "_blank";
                linkElem.innerText = text;

                elem = createBlock({ elem: 'p' });
                elem.appendBlock(linkElem);
            } else if (prefix.includes('kontakt')) {
                elem = createBlock({ elem: 'p' });
                if (key.endsWith('.town')) {
                    elem.innerHTML = `<b>${text}</b>`;
                } else {
                    elem.setText(text);
                }
            } else {
                elem = createBlock({ elem: 'div' });
                elem.setText(text);
            }

            elem.className = 'col';

            const containerDiv = createBlock({ elem: 'div', cls: 'col' });
            containerDiv.appendBlock(elem);

            elementsArray.push({key: key.split('.').pop(), elem: containerDiv});
        }
    }

    if (prefix.includes('kontakt')) {
        elementsArray.sort((a, b) => {
            return sortOrder.indexOf(a.key) - sortOrder.indexOf(b.key);
        });
    }

    for (const { elem } of elementsArray) {
        targetElement.append(elem);
    }
};

var doAlert = function (txt) {
    alert(txt)
}