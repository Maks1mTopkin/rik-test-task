document.getElementById("newEvent").addEventListener("submit", function(event) {
    event.preventDefault();

    var name = document.getElementById("name");
    var eventTime = document.getElementById("eventTime");
    var location = document.getElementById("location");
    var additionalInfo = document.getElementById("additionalInfo");

    var args = {
        method: "POST",
        url: "/addEvent",
        params: {
            name: name.value,
            eventTime: eventTime.value,
            location: location.value,
            additionalInfo: additionalInfo.value
        }
    };

    doRequest(args, function(response) {});

    name.value = "";
    eventTime.value = "";
    location.value = "";
    additionalInfo.value = "";
});

var getFlatpickr = function () {
    var eventTime = document.getElementById('eventTime');
    flatpickr(eventTime, {
        enableTime: true,
        noCalendar: false,
        dateFormat: "d.m.Y H:i:S",
        time_24hr: true,
        minDate: "today",
        onOpen: function(selectedDates, dateStr, instance) {
            instance.calendarContainer.style.top = (instance.element.getBoundingClientRect().bottom + window.scrollY) + "px";
            instance.calendarContainer.style.left = (instance.element.getBoundingClientRect().left + window.scrollX) + "px";
        },
    });
}

document.addEventListener("DOMContentLoaded", function() {
    var eventTime = document.getElementById('eventTime');
    console.log(eventTime)
    debugger
    flatpickr(eventTime, {
        enableTime: true,
        noCalendar: false,
        dateFormat: "d.m.Y H:i:S",
        time_24hr: true,
        minDate: "today",
    });
});
