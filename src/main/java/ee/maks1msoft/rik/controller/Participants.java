package ee.maks1msoft.rik.controller;

import ee.maks1msoft.rik.model.CompanyParticipant;
import ee.maks1msoft.rik.model.IndividualParticipant;
import ee.maks1msoft.rik.repository.CompanyParticipantRepository;
import ee.maks1msoft.rik.repository.IndividualParticipantRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;

import ee.maks1msoft.rik.service.EventService;

import java.util.ArrayList;
import java.util.List;

@Controller
public class Participants extends BaseController {

    @Autowired
    private EventService eventService;

    @Autowired
    private CompanyParticipantRepository companyParticipantRepository;

    @Autowired
    private IndividualParticipantRepository individualParticipantRepository;



    @GetMapping("/osavotjad")
    public String contactPage(Model model) {
        addCommonAttributes(model);
        return "osavotjad";
    }

    @PostMapping("/{eventId}/addCompanyParticipant")
    public ResponseEntity<?> addCompanyParticipant(@PathVariable Long eventId, @RequestBody CompanyParticipant companyParticipant) {
        eventService.addCompanyParticipantToEvent(eventId, companyParticipant);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/{eventId}/addIndividualParticipant")
    public ResponseEntity<?> addIndividualParticipant(@PathVariable Long eventId, @RequestBody IndividualParticipant individualParticipant) {
        eventService.addIndividualParticipantToEvent(eventId, individualParticipant);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/osavotjad/{eventId}")
    public List<Object> getParticipants(@PathVariable Long eventId) {
        List<CompanyParticipant> companyParticipants = companyParticipantRepository.findByEvent_Id(eventId);
        List<IndividualParticipant> individualParticipants = individualParticipantRepository.findByEvent_Id(eventId);

        List<Object> allParticipants = new ArrayList<>();

        allParticipants.addAll(companyParticipants);
        allParticipants.addAll(individualParticipants);

        return allParticipants;
    }
}
