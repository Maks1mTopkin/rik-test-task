package ee.maks1msoft.rik.model;

import lombok.Data;

@Data
public class EventDto {
    private String name;
    private String eventTime;
    private String location;
    private String additionalInfo;


}
