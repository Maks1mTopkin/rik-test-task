package ee.maks1msoft.rik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RikApplication {

	public static void main(String[] args) {
		SpringApplication.run(RikApplication.class, args);
	}

}
