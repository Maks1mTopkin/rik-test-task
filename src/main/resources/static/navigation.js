var setDynamicNavigation = function () {
    var CURRENT_LOCATION = window.location.pathname.substring(1);


    var navigationBlock = document.getElementById('navText');

    if (navigationBlock) {
        var textElem = createBlock({elem: 'p'});
        textElem.setText(getNavigationMessage(CURRENT_LOCATION));
        navigationBlock.appendChild(textElem);
    }

}