package ee.maks1msoft.rik.service;

public class validateIsikukood {

    public boolean validateIsikukood(String code) {
        if (code == null || !code.matches("^[1-6]\\d{10}$")) {
            return false;
        }

        int year = Integer.parseInt(code.substring(1, 3));
        int month = Integer.parseInt(code.substring(3, 5));
        int day = Integer.parseInt(code.substring(5, 7));

        if (month < 1 || month > 12 || day < 1 || day > 31) {
            return false;
        }

        int sum = 0;
        for (int i = 0; i < code.length(); i++) {
            sum += Character.getNumericValue(code.charAt(i)) * (i + 1);
        }

        if (sum % 11 == 10) {
            sum = 0;
            for (int i = 0; i < code.length(); i++) {
                sum += Character.getNumericValue(code.charAt(i)) * (i + 3);
            }
        }

        return sum % 11 == 0;
    }

}
