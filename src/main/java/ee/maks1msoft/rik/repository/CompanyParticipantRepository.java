package ee.maks1msoft.rik.repository;

import ee.maks1msoft.rik.model.CompanyParticipant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CompanyParticipantRepository extends JpaRepository<CompanyParticipant, Long> {
    List<CompanyParticipant> findByEvent_Id(Long eventId);
}
