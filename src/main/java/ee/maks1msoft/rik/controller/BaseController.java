package ee.maks1msoft.rik.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;


public class BaseController {

    @Value("${projectName}")
    protected String projectName;

    protected void addCommonAttributes(Model model) {
        model.addAttribute("projectName", projectName);
    }

}
