
var setMainPageTexts = function () {
    document.getElementById("futureEntertainments").textContent = getMainPageMessage('future_entertainments');
    document.getElementById("occurredEntertainments").textContent = getMainPageMessage('occurred_entertainments');
    document.getElementById("addEntertainment").textContent = getMainPageMessage('add_entertainment');

    getEvents();
}

var events = {};
var getEvents = function () {
    doRequest({url: 'api/getAllEvents'}, function (data) {
        fillContent(data)
    });
}

var fillContent = function (content) {
    var futureEntertainmentsData = document.getElementById("futureEntertainmentsData");
    var occurredEntertainmentsData = document.getElementById("occurredEntertainmentsData");
    var futureIdx = 1;
    var occuredIdx = 1;
    const currentDate = new Date();

    content.forEach((item) => {
        const eventDate = new Date(item.eventTime.split('.').reverse().join('-'));
        // console.log(`Name: ${item.name}, Event Time: ${item.eventTime}`)
        if (eventDate < currentDate) {
            createDataBlock(occuredIdx, item.id, item.name, item.eventTime, occurredEntertainmentsData)
            occuredIdx++;
        } else {
            createDataBlock(futureIdx, item.id, item.name, item.eventTime, futureEntertainmentsData)
            futureIdx++;
        }
    })
}

var createDataBlock = function (idx, id, name, eventTime, parrent) {
    var mainDataBlock = createBlock({cls: 'd-flex align-items-center w-100'}); // col d-flex justify-content-center align-items-start futureEntertainments
    var eventNr = createBlock({cls: 'order-number'});
    var eventName = createBlock({cls: 'event-name flex-grow-1'});
    var eventDate = createBlock({cls: 'event-date'});
    var eventAction = createBlock({cls: 'event-action'});
    var actionShowParticipants = createBlock({elem: 'a', cls: 'btn btn-link participants'});
    var deleteBlock = createBlock({cls: 'delete-button'});
    var deleteBLockAction = createBlock({elem: 'img', cls: 'delete-event'})
    deleteBLockAction.setAttribute('src', 'remove.svg');
    deleteBLockAction.setAttribute('alt', 'delete');

    eventNr.setText(idx.toString());
    eventName.setText(idx.toString() + "." +  name);
    eventDate.setText(eventTime);
    actionShowParticipants.setText("Osavotad");
    actionShowParticipants.setAttribute("href", "http://localhost:8080/osavotjad");
    deleteBLockAction.setAttribute("data-id", id);

    // mainDataBlock.appendBlock(eventNr);
    mainDataBlock.appendBlock(eventName);
    mainDataBlock.appendBlock(eventDate);
    eventAction.appendBlock(actionShowParticipants);
    mainDataBlock.appendBlock(eventAction);
    deleteBlock.appendBlock(deleteBLockAction);
    mainDataBlock.appendBlock(deleteBlock);

    parrent.appendChild(mainDataBlock);
}


document.addEventListener('click', function(event) {
    if (event.target.classList.contains('delete-event')) {
        const dataId = event.target.getAttribute('data-id');
        doRequest({
            method: "POST",
            url: "/deleteEvent/" + dataId,
            params: {}
        }, function(response) {
            console.log(response)
                location.reload();
        });
    }
});