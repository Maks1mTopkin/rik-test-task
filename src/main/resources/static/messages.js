var bundle = {
    headerTxt: {},
    footerTxt: {},
    navigationTxt: {},
    mainPageTxt: {}
};

var messageBundle = {
    getHeaderMessage: function (key) {
        return key !== null && key !== undefined ? bundle.headerTxt[key] : '';
    },
    getFooterMessage: function (key) {
        return key !== null && key !== undefined ? bundle.footerTxt[key] : '';
    },
    getNavigationMessage: function (key) {
        return key !== null && key !== undefined ? bundle.navigationTxt[key] : '';
    },
    getMainPageMessage: function (key) {
        return key !== null && key !== undefined ? bundle.mainPageTxt[key] : '';
    }
};

var getHeaderMessage = function (key) {
    return messageBundle.getHeaderMessage(key);
};

var getFooterMessage = function (key) {
    return messageBundle.getFooterMessage(key);
};

var getNavigationMessage = function (key) {
    return messageBundle.getNavigationMessage(key);
};

var getMainPageMessage = function (key) {
    return messageBundle.getMainPageMessage(key);
};


doRequest({url: "api/messages", params: {section : "header"}}, function (data) {
    bundle.headerTxt = data;
    setHeaderTexts();
});
doRequest({url: "api/messages", params: {section : "footer"}}, function (data) {
    bundle.footerTxt = data;
    createFooter();
});
doRequest({url: "api/messages", params: {section : "navigation"}}, function (data) {
    bundle.navigationTxt = data;
    setDynamicNavigation();
});
doRequest({url: "api/messages", params: {section : "main_page"}}, function (data) {
    bundle.mainPageTxt = data;
    setMainPageTexts();
});

doRequest({url: "api/messages", params: {section : "urituse_lisamine"}}, function (data) {
    getFlatpickr();
});